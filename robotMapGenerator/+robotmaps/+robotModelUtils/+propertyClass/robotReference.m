classdef robotReference < handle
    %ROBOTREFERENCE Class that records the reference points of the robot
    %   - record the reference point of the robot
    
    properties (SetAccess=immutable)
        % the rigidBodyTree model of the robot. Normally retrieved by loadrobot()
        robotColumn(1,1) rigidBodyTree {mustBeColumnDataFormat(robotColumn)}

        % the tag of the rigidBodyTree. Could be used to distinguish between
        % different modifications on the same robot model.
        tag(1,:) char

        % the robot model name.
        robotName(1,:) char
    end

    properties (SetObservable)
        % the base reference link-frame name
        baseName(1,:) char

        % the shoulder reference link-frame name 
        shoulderName(1,:) char

        % the elbow reference link-frame name
        elbowName(1,:) char

        % the EE reference link-frame name
        endEffectorName(1,:) char
    end

    properties
        % the number of sections for nullspace motion
        nSection(1,1) double {mustBeInteger(nSection),mustBePositive(nSection)} = 1
    end
    
    % constructor
    methods
        function obj = robotReference(robotColumn,tag,robotName)
        % robotReference constructor
            obj.robotColumn=robotColumn;
            obj.tag = tag;
            obj.robotName = robotName;
            addlistener(obj,'baseName','PostSet',@obj.mustBeValidReferenceFrame);
            addlistener(obj,'shoulderName','PostSet',@obj.mustBeValidReferenceFrame);
            addlistener(obj,'elbowName','PostSet',@obj.mustBeValidReferenceFrame);
            addlistener(obj,'endEffectorName','PostSet',@obj.mustBeValidReferenceFrame);
        end
    end

    % list of observer method
    methods (Static) 
        function mustBeValidReferenceFrame(src,evnt)
            % validation function on the link frame name
            % the reference name should be a name of rigidBody in the model robotColumn
            if ~isempty(evnt.AffectedObject.robotColumn)
                if ~isempty(evnt.AffectedObject.(src.Name))
                   totalName =  [evnt.AffectedObject.robotColumn.BaseName, evnt.AffectedObject.robotColumn.BodyNames];
                   mustBeMember(evnt.AffectedObject.(src.Name),totalName);
                end
            end
        end
    end
end

function mustBeColumnDataFormat(robotColumn)
    % validation function on robotColumn
    % it should be in DataFormat of column
    if robotColumn.NumBodies>0
        DataFormat = robotColumn.DataFormat;
        if ~isequal(DataFormat,'column')
            error('\nThe Dataformat of the rigidBodyTree should be ''column'' instead of ''%s''',DataFormat);
        end
    end
end