classdef robotModel < handle
    %ROBOTMODEL class to store the robot model information
    %  the mission of this class includes:
    %  - store the reference point of the robot.
    %  - validate if these reference input are correct.
    %
    % it could be copied to a robotMapClass to be used as property
    
    properties
        % the robotReference property. Records the robotModel and some of
        % the reference points
        robotReference(1,1)
        
    end
    
    % constructor
    methods
        function obj = robotModel(robotReference)
            % Constructor of robotModel 
            % construct the robotModel with the robotReference Information
            % and the manage the factory type. Initialize the class with
            % numeric Normal mode as default.

            obj.robotReference = robotReference;
            
           
        end
    end


    % list of get interface
    methods        
        % solve the inverse Kinematics for robotmodel
        % endEffectorName, weight, repeat are optional arguments.
        [configSol,solInfo] = ikSolve(obj,tform,initialguess,endEffectorName,weight,repeat)
        
        % output the adapter for inverse Kinematics
        % could be used in parallel computation.
        % endEffectorName, weight, repeat are optional arguments.
        ikAdapter = ikSolveAdapter(obj,endEffectorName,weight,repeat)

        % 
        jacobian = jacobianMatrix(obj,config,endEffectorName)

        %
        jHandle = jacobianMatrixHandle(obj,endEffectorName,options)

        %
        [angle,section] = nullspaceReference(obj,configSol)
        
        %
        isReachable = isNullspaceReachable(obj,configSolOld,configSolNew)

        %
        flip = flipReference(obj,configSol)

        %
        configOut = closeLoopStep(obj,configIn,Solver,step)

        %
        configOut = nullspaceStep(obj,configIn,Solver,step,direction)
    end
end

